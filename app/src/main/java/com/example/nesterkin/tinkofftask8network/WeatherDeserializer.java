package com.example.nesterkin.tinkofftask8network;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * @author Nesterkin Alexander on 27/11/2018
 */
public class WeatherDeserializer implements JsonDeserializer<Weather> {

    @Override
    public Weather deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String description = json.getAsJsonObject().getAsJsonArray("weather").get(0).getAsJsonObject().getAsJsonPrimitive("description").getAsString();
        float temp = json.getAsJsonObject().getAsJsonObject("main").getAsJsonPrimitive("temp").getAsFloat();
        float speedWind = json.getAsJsonObject().getAsJsonObject("wind").getAsJsonPrimitive("speed").getAsFloat();
        return new Weather(description,
                new Date().getTime(),
                temp,
                speedWind
        );
    }
}