package com.example.nesterkin.tinkofftask8network;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Nesterkin Alexander on 27/11/2018
 */
public class WeatherListDeserializer implements JsonDeserializer<List<Weather>> {

    @Override
    public List<Weather> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<Weather> result = new ArrayList<>();
        JsonArray array = ((JsonObject) json).getAsJsonArray("list");
        long time = new Date().getTime();
        for (JsonElement jsonElement : array) {
            String description = jsonElement.getAsJsonObject().getAsJsonArray("weather").get(0).getAsJsonObject().getAsJsonPrimitive("description").getAsString();
            float temp = jsonElement.getAsJsonObject().getAsJsonObject("main").getAsJsonPrimitive("temp").getAsFloat();
            float speedWind = jsonElement.getAsJsonObject().getAsJsonObject("wind").getAsJsonPrimitive("speed").getAsFloat();
            result.add(new Weather(description,
                    time,
                    temp,
                    speedWind));
        }
        return result;
    }
}