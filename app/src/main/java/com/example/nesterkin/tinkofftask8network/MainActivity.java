package com.example.nesterkin.tinkofftask8network;

import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static final String[] CITIES = new String[]{"Москва", "Сочи", "Владивосток", "Челябинск"};
    private static final String[] QUERY_CITIES = new String[]{"Moscow,RU", "Sochi,RU", "Vladivostok,RU", "Chelyabinsk,RU"};

    private WeatherApi mWeatherApi;
    private CompositeDisposable mCompositeDisposable;

    private Spinner mCitySpinner;
    private Button mLoadWeatherButton;
    private TextView mResultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mWeatherApi = ((WeatherApp) getApplication()).getApi();

        mCompositeDisposable = new CompositeDisposable();

        mCitySpinner = findViewById(R.id.city_spinner);
        mLoadWeatherButton = findViewById(R.id.load_weater_button);
        mResultTextView = findViewById(R.id.result_text_view);

        ArrayAdapter<String> citiesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, CITIES);
        mCitySpinner.setAdapter(citiesAdapter);

        mLoadWeatherButton.setOnClickListener(view -> performRequest());

        mCompositeDisposable.add(new NetworkConnectionManager(this).getAvailable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(enabled -> mLoadWeatherButton.setEnabled(enabled)));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
    }

    private void performRequest() {
        String queryCity = QUERY_CITIES[mCitySpinner.getSelectedItemPosition()];
        mCompositeDisposable.add(Single.zip(mWeatherApi.getWeather(queryCity),
                mWeatherApi.getForecast(queryCity), Pair::new)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pair -> outputResult(pair.first, pair.second),
                        throwable -> mResultTextView.setText(throwable.getMessage())));
    }

    private void outputResult(Weather weather, List<Weather> forecast) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(weather.toString());

        stringBuilder.append("\n\nПрогноз на день:\n\n");

        for (int i = 0; i < 8; i++) {
            Weather forecast1 = forecast.get(i);
            stringBuilder.append(forecast1.toString());
            stringBuilder.append("\n");
        }

        mResultTextView.setText(stringBuilder.toString());
    }
}