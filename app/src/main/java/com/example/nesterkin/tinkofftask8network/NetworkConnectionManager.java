package com.example.nesterkin.tinkofftask8network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * @author Nesterkin Alexander on 27/11/2018
 */
public class NetworkConnectionManager {

    private BehaviorSubject<Boolean> mBehaviorSubject;

    NetworkConnectionManager(Context context) {
        mBehaviorSubject = BehaviorSubject.create();

        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        mBehaviorSubject.onNext(networkInfo != null && networkInfo.isConnected());

        ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                mBehaviorSubject.onNext(true);
            }

            @Override
            public void onLost(Network network) {
                mBehaviorSubject.onNext(false);
            }
        };

        NetworkRequest networkRequest = new NetworkRequest.Builder()
                .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .build();

        connectivityManager.registerNetworkCallback(networkRequest, networkCallback);

    }

    public Observable<Boolean> getAvailable() {
        return mBehaviorSubject;
    }
}