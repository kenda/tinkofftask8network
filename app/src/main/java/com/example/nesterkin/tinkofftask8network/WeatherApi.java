package com.example.nesterkin.tinkofftask8network;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Nesterkin Alexander on 27/11/2018
 */
public interface WeatherApi {

    @GET("weather?units=metric&APPID=7910f4948b3dcb251ebc828f28d8b30b")
    Single<Weather> getWeather(@Query("q") String city);

    @GET("forecast?units=metric&APPID=7910f4948b3dcb251ebc828f28d8b30b")
    Single<List<Weather>> getForecast(@Query("q") String city);
}