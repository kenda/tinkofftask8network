package com.example.nesterkin.tinkofftask8network

/**
 * @author Nesterkin Alexander on 27/11/2018
 */
data class Weather(val description: String,
                   val time: Long,
                   val temperature: Float,
                   val windSpeed: Float)